'use strict';
module.exports = (sequelize, DataTypes) => {
  const payment_cycles = sequelize.define('payment_cycles', {
    name: DataTypes.STRING
  }, {});
  payment_cycles.associate = function(models) {
    // associations can be defined here
  };
  return payment_cycles;
};