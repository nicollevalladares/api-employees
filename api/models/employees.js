'use strict';
module.exports = (sequelize, DataTypes) => {
  const employees = sequelize.define('employees', {
    name: DataTypes.STRING,
    birth_date: DataTypes.DATEONLY,
    bank_account: DataTypes.STRING,
    bank: DataTypes.STRING,
    contract_type: DataTypes.STRING,
    job: DataTypes.STRING,
    payment_cycle_id: DataTypes.INTEGER,
    turn: DataTypes.STRING,
    id_number: DataTypes.STRING,
    phone_number: DataTypes.STRING,
    rap_number: DataTypes.STRING,
    rtn_number: DataTypes.STRING
  }, {});
  employees.associate = function(models) {
    // associations can be defined here
    employees.belongsTo(models.payment_cycles, {
      foreignKey: 'payment_cycle_id'
    });
  };
  return employees;
};