const employees = require('../models').employees;
const payment_cycles = require('../models').payment_cycles;
// const employeesRole = require('../models').employeesRole;

module.exports = {
    list(req, res) {
        try {
            return employees
            .findAll({
                include: [{
                    model: payment_cycles,
                    as: 'payment_cycle'
                }]
            })
            .then((Employee) => {
                res.status(200).send(Employee)
            })
            .catch((error) => {
                res.status(400).send(error); 
            });
        } catch (error) {
            res.status(400).send(error);
        }
    },

    getById(req, res) {
        try {
            return employees
            .findByPk(req.params.id, {
                include: [{
                    model: payment_cycles,
                    as: 'payment_cycle'
                }],
            })
            .then((employees) => {
                if (!employees) {
                    return res.status(404).send({
                        message: 'employees Not Found',
                    });
                }
                return res.status(200).send(employees);
            })
            .catch((error) => {
                res.status(400).send(error)
        });
        } catch (error) {
            res.status(400).send(error);
        }
    },

    add(req, res) {
        try {
            return employees
            .create({
                name: req.body.name,
                birth_date: req.body.birth_date,
                bank_account: req.body.bank_account,
                bank: req.body.bank,
                contract_type: req.body.contract_type,
                job: req.body.job,
                turn: req.body.turn,
                payment_cycle_id: req.body.payment_cycle_id,
                id_number: req.body.id_number,
                phone_number: req.body.phone_number,
                rap_number: req.body.rap_number,
                rtn_number: req.body.rtn_number
            })
            .then((employees) => {
                res.status(201).send(employees)
            })
            .catch((error) => {
                res.status(400).send(error)
            });
        } catch (error) {
            res.status(400).send(error);
        }
    },

    update(req, res) {
        try {
            return employees
            .findByPk(req.params.id, {
                include: [{
                    model: payment_cycles,
                    as: 'employees'
                }],
            })
            .then(employees => {
                if (!employees) {
                    return res.status(404).send({
                        message: 'employees Not Found',
                    });
                }
                employees
                .update({
                    name: req.body.name || employees.name,
                    birth_date: req.body.birth_date || employees.birth_date,
                    bank_account: req.body.bank_account || employees.bank_account,
                    bank: req.body.bank || employees.bank,
                    contract_type: req.body.contract_type || employees.contract_type,
                    job: req.body.job || employees.job,
                    payment_cycle_id: req.body.payment_cycle_id || employees.payment_cycle_id,
                    id_number: req.body.id_number || employees.id_number,
                    phone_number: req.body.phone_number || employees.phone_number,
                    rap_number: req.body.rap_number || employees.rap_number,
                    rtn_number: req.body.rtn_number || employees.rtn_number
                })
                .then(() => {
                    res.status(200).send(employees)
                })
                .catch((error) => {
                    res.status(400).send(error)
                });
            })
            .catch((error) => {
                res.status(400).send(error)
            });
        } catch (error) {
            res.status(400).send(error);
        }
    },

    delete(req, res) {
        try {
            return employees
            .findByPk(req.params.id)
            .then(employees => {
                if (!employees) {
                return res.status(400).send({
                    message: 'employees Not Found',
                });
                }
                employees
                .destroy()
                .then(() => {
                    res.status(204).send()
                })
                .catch((error) => {
                    res.status(400).send(error)
                });
            })
            .catch((error) => {
                res.status(400).send(error)
            });
        } catch (error) {
            res.status(400).send(error);
        }
    }
};