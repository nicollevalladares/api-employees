const payment_cycles = require('../models').payment_cycles;
const employees = require('../models').employees;

module.exports = {
    list(req, res) {
        return payment_cycles
        .findAll()
        .then((cycle) => {
            res.status(200).send(cycle)
        })
        .catch((error) => {
            res.status(400).send(error); 
        });
    },

    getById(req, res) {
        return payment_cycles
        .findByPk(req.params.id)
        .then((payment_cycles) => {
            if (!payment_cycles) {
                return res.status(404).send({
                    message: 'payment_cycles Not Found',
                });
            }
            return res.status(200).send(payment_cycles);
        })
        .catch((error) => {
            res.status(400).send(error)
        });
    },

    add(req, res) {
        return payment_cycles
        .create({
            name: req.body.name || payment_cycles.name
        })
        .then((payment_cycles) => {
            res.status(201).send(payment_cycles)
        })
        .catch((error) => {
            res.status(400).send(error)
        });
    },

    update(req, res) {
        return payment_cycles
        .findByPk(req.params.id)
        .then(payment_cycles => {
            if (!payment_cycles) {
                return res.status(404).send({
                    message: 'payment_cycles Not Found',
                });
            }
            return payment_cycles
            .update({
                name: req.body.name
            })
            .then(() => {
                res.status(200).send(payment_cycles)
            })
            .catch((error) => {
                res.status(400).send(error)
            });
        })
        .catch((error) => {
            res.status(400).send(error)
        });
    },

    delete(req, res) {
        return payment_cycles
        .findById(req.params.id)
        .then(payment_cycles => {
            if (!payment_cycles) {
            return res.status(400).send({
                message: 'payment_cycles Not Found',
            });
            }
            return payment_cycles
            .destroy()
            .then(() => {
                res.status(204).send()
            })
            .catch((error) => {
                res.status(400).send(error)
            });
        })
        .catch((error) => {
            res.status(400).send(error)
        });
    }
};