const employees = require('./employees');
const payment_cycles = require('./payment_cycles');

module.exports = {
    employees,
    payment_cycles,
};