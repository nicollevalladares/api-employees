var express = require('express');
var router = express.Router();

const payment_cycles_controller = require('../controllers').payment_cycles;
const employees_controller = require('../controllers').employees;

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

/* EMployees Router */
router.get('/employees', employees_controller.list);
router.get('/employees/:id', employees_controller.getById);
router.post('/employees', employees_controller.add);
router.put('/employees/:id', employees_controller.update);
router.delete('/employees/:id', employees_controller.delete);

/* Payment Cycles Router */
router.get('/payment_cycles', payment_cycles_controller.list);
router.get('/payment_cycles/:id', payment_cycles_controller.getById);
router.post('/payment_cycles', payment_cycles_controller.add);
router.put('/payment_cycles/:id', payment_cycles_controller.update);
router.delete('/payment_cycles/:id', payment_cycles_controller.delete);


module.exports = router;