'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('employees', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      name: {
        type: Sequelize.STRING
      },
      birth_date: {
        type: Sequelize.DATEONLY
      },
      bank_account: {
        type: Sequelize.STRING
      },
      bank: {
        type: Sequelize.STRING
      },
      contract_type: {
        type: Sequelize.STRING
      },
      job: {
        type: Sequelize.STRING
      },
      turn: {
        type: Sequelize.STRING
      },
      payment_cycle_id: {
        type: Sequelize.INTEGER
      },
      id_number: {
        type: Sequelize.STRING
      },
      phone_number: {
        type: Sequelize.STRING
      },
      rap_number: {
        type: Sequelize.STRING
      },
      rtn_number: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('employees');
  }
};